package fr.cnam.foad.nfa035.exam.model;


import java.util.Date;
import java.util.Objects;

public class MyWhiteExam {

    private String intitule;
    private String codeUe;
    private String ecole;
    private String region;
    private Date date;

    public MyWhiteExam() {
    }

    public MyWhiteExam(String intitule, String codeUe, String ecole, String region, Date date) {
        // A Faire
    }

   // A compléter


   
    

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "MyExam{" +
                "intitule='" + intitule + '\'' +
                ", codeUe='" + codeUe + '\'' +
                ", ecole='" + ecole + '\'' +
                ", region='" + region + '\'' +
                ", date=" + date.getTime() +
                '}';
    }

}
