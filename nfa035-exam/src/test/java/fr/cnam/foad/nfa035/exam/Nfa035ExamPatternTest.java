package fr.cnam.foad.nfa035.exam;


import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import fr.cnam.foad.nfa035.exam.model.MyWhiteExam;
import fr.cnam.foad.nfa035.exam.pattern.MyExamDelegate;


@ContextConfiguration(classes = { Nfa035ExamConfiguration.class })
@RunWith(SpringRunner.class)
@SpringBootTest
public class Nfa035ExamPatternTest {

    @Autowired
    MyExamDelegate delegate;

    @Autowired
    MyWhiteExam currentExam;

    /**
     * Documentez-moi
     *
     * @throws Exception
     */
    @Test
    public void testSingletonFactory() throws Exception {
        assertEquals(currentExam, new MyWhiteExam("Bibliotheques et Patterns", "NFA035", "CNAM", "Ile de France", currentExam.getDate()));
    }


    /**
     * Documentez-moi
     *
     * @throws Exception
     */
    @Test
    public void testDelegateToString() throws Exception {
        String toString = delegate.delegateToString();
        System.out.println(toString);
        assertEquals(currentExam.toString(), toString);
    }



}